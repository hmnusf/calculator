import calc_helper as ch

if __name__=='__main__':
    print('Kalkulator')
    print('1.Dodaj')
    print('2.Odejmij')
    print('3.Pomnóż')
    print('4.Podziel')
    print('0.Wyjście')
    while True:
        try:
            a=float(input('Podaj pierwszą liczbę'))
            b=float(input('Podaj drugą liczbę'))
            choice=int(input('Co chcesz z nimi zrobić:'))
            if choice==1:
                print(f'Wynik={ch.add(a,b)}')
            elif choice==2:
                print(f'Wynik={ch.sub(a,b)}')
            elif choice==3:
                print(f'Wynik={ch.multi(a,b)}')
            elif choice==4:
                print(f'Wynik={ch.div(a,b)}')
            elif choice==0:
                break
        except:
            print('Jedna z liczb została źle podana!')